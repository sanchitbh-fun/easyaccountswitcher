(function () {
    angular.module('ngApp', []).controller('ngCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.msg = '';
        $scope.video_id = '0TjxnrWT8Es';
        $scope.donate_link = 'http://www.paypal.com';

        $scope.init = function () {
            var defaults = {profiles: [], domains: 'google|youtube', reload: 'tab', proto: 'https', custom_url: '', install_date: null, pro: false};

            chrome.storage.local.get(defaults, function (result) {
                //console.log("result: ", result);
                $timeout(function () {
                    $scope.profiles = result.profiles;
                    $scope.domainRegEx = new RegExp(result.domains, 'i');
                    $scope.reload = result.reload;
                    $scope.proto = result.proto;
                    $scope.custom_url = result.custom_url;
                    $scope.expired = (((new Date()).getTime() - (new Date(result.install_date)).getTime()) / (1000 * 3600 * 24)) > 30;
                    $scope.pro = result.pro;

                    if ($scope.pro) { //Hello fellow coder, you can set this to true and get a free upgrade Or you can donate $2 for the time I spent writing it and get the upgrade with some good karma :)
                        $scope.expired = false;
                    }
                });
            });

            $timeout(function () {
                $('[data-toggle="tooltip"]').tooltip({container: 'body'});
            }, 1000);
        };

        $scope.load = function (profile, index, site) {
            if ($scope.expired && (index > 1)) {
                if (confirm("Your free trial has expired. In the basic version you can only use the first two profiles. To unlock the full version, please upgrade your account. " +
                        "You can donate any amount like $2, $5, etc to get the full version and unlock all features.\n\nWould you like to upgrade now?")) {
                    window.open($scope.donate_link);
                }
            } else if (profile.cookies) {
                var cookies = JSON.parse(profile.cookies);
                var domains = [];

                angular.forEach(cookies, function (cookie) {
                    domains.push(cookie.domain.replace('.', '\.'));
                });

                $scope.clearCookies(domains.join('|'), function () {
                    angular.forEach(cookies, function (cookie) {
                        delete(cookie.hostOnly);
                        delete(cookie.session);
                        cookie.url = "https://" + cookie.domain + cookie.path;
                        chrome.cookies.set(cookie);
                    });

                    if (typeof site === 'undefined') {
                        $scope.reloadTab();
                    } else {
                        chrome.tabs.getSelected(null, function (tab) {
                            var sameDomain = new RegExp(site.replace(/(www|mail|drive)\./, '').replace('.', '\\.'), 'i');

                            if (sameDomain.test(tab.url)) {
                                chrome.tabs.executeScript(tab.id, {code: 'window.location.href="https://' + site + '"'});
                            } else {
                                window.open('https://' + site);
                            }
                        });
                    }

                    $scope.alert('Cookies successfully loaded', true);
                });
            }
        };

        $scope.reloadTab = function () {
            if ($scope.reload) {
                if ($scope.custom_url && ($scope.reload === 'custom')) {
                    chrome.tabs.getSelected(null, function (tab) {
                        chrome.tabs.executeScript(tab.id, {code: 'window.location.href="' + $scope.custom_url + '"'});
                    });
                } else if ($scope.reload === 'tab') {
                    chrome.tabs.getSelected(null, function (tab) {
                        chrome.tabs.executeScript(tab.id, {code: 'window.location.reload();'});
                    });
                } else if ($scope.reload === 'all') {
                    chrome.tabs.getAllInWindow(null, function (tabs) {
                        angular.forEach(tabs, function (tab) {
                            if ($scope.domainRegEx.test(tab.url)) {
                                chrome.tabs.executeScript(tab.id, {code: 'window.location.reload();'});
                            }
                        });
                    });
                }
            }
        };

        $scope.save = function (profile) {
            chrome.cookies.getAll({}, function (cookies) {
                profile.cookies = JSON.stringify(cookies);

                chrome.storage.local.set({profiles: $scope.profiles}, function (result) {
                    $scope.alert('Profile successfully updated');
                });
            });
        };

        $scope.clear = function (profile) {
            profile.cookies = [];
            chrome.storage.local.set({profiles: $scope.profiles}, function (result) {
                $timeout(function () {});
            });
        };

        $scope.newSession = function () {
            $scope.clearCookies(null, $scope.reloadTab);
        };

        $scope.getcode = function () {
            if (value = prompt('Enter the unlock code you have received in your email:', 'paste code here')) {
                if (/^\s*YOUROCK\s*$/i.test(value)) {
                    chrome.storage.local.set({pro: true}, function () {
                        $scope.alert('Account fully upgraded. Thank you for your support. You rock! :)');
                        $timeout(function () {$scope.expired = false;});
                    });
                } else {
                    $scope.alert('Sorry, that code is not valid');
                }
            }
        };

        $scope.alert = function (msg, closePopup) {
            $timeout(function () {$scope.msg = msg;});
            $timeout(function () {
                $scope.msg = '';

                if (closePopup) {
                    self.close();
                }
            }, 2500);
        };

        $scope.clearCookies = function (pattern, cb) {
            var regex = pattern ? new RegExp(pattern, 'i') : null;
            //console.log("regex: ", regex);
            chrome.cookies.getAll({}, function (cookies) {
                angular.forEach(cookies, function (cookie) {
                    if (!regex || regex.test(cookie.domain)) {
                        chrome.cookies.remove({url: "https://" + cookie.domain + cookie.path, name: cookie.name}, function (done) {
                            //console.log("removed: ", done);
                        });
                    }
                });

                if (cb) {
                    cb(cookies);
                }
            });
        };

        $scope.init();
    }]);
})();