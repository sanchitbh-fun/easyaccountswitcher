angular.module('ngOptions', [])
    .controller('ngOptionsController', function ($scope, $timeout) {
        var defaults = {
            profiles: [{name: '', cookies: []}],
            domains: 'google|youtube|facebook|twitter|amazon|reddit',
            reload: 'all',
            proto: 'https',
            advanced: false,
            custom_url: 'https://mail.google.com',
            install_date: (new Date()).toString()
        };

        $scope.video_id = '0TjxnrWT8Es';

        chrome.storage.local.get(defaults, function (result) {
            console.log("result: ", result);
            $timeout(function () {$scope.settings = result;});
        });


        $scope.save = function () {
            console.log("$scope.settings: ", $scope.settings);
            chrome.storage.local.set($scope.settings, function (result) {
                $timeout(function () {$scope.msg = 'Profiles updated!';});
                $timeout(function () {$scope.msg = ''}, 2000);
            });
        };

        $scope.clear = function () {
            chrome.storage.local.clear();
            location.reload();
        };
    });
